var img = new Image();
img.setAttribute('crossOrigin', '');
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var canvasWithoutPng = document.getElementById('canvasWithoutPng');
var ctx2 = canvasWithoutPng.getContext('2d');

function isMobile() {
  return (
    (navigator.userAgent.match(/Android/i)) ||
    (navigator.userAgent.match(/webOS/i)) ||
    (navigator.userAgent.match(/iPhone/i)) ||
    (navigator.userAgent.match(/iPod/i)) ||
    (navigator.userAgent.match(/iPad/i)) ||
    (navigator.userAgent.match(/BlackBerry/i))
  );
}
if (document.readyState === 'complete') {
  if (isMobile()) {
    document.getElementById('contenedor').style.flexDirection = "column"
  }
}
function loadFile(event) {
  img.src = URL.createObjectURL(event.target.files[0]);
  img.onload = function () {
    canvas.height = img.height
    canvas.width = img.width
    canvasWithoutPng.height = img.height
    canvasWithoutPng.width = img.width
    ctx.drawImage(img, 0, 0);
    ctx2.drawImage(img, 0, 0);
  };
};
function clickMe() {
  //
  const box = document.getElementById('grey').checked
  const rageGrey = parseInt(document.getElementById('rageGrey').value)
  //
  let imageData = ctx.getImageData(0,0,canvas.width,canvas.height)
  let pixels = imageData.data
  const numPixels = imageData.width * imageData.height;
  for (var i = 0; i < numPixels; i++) {
    const r = pixels[i * 4];
    const g = pixels[i * 4 + 1];
    const b = pixels[i * 4 + 2];
    var grey = (r + g + b) / 3;
    if(box){
      pixels[i * 4] =pixels[i * 4 +1]= pixels[i * 4 +2] = grey
    }else{
      pixels[i * 4] = pixels[i * 4];
      pixels[i * 4 + 1] = pixels[i * 4 +1];
      pixels[i * 4 + 2] = pixels[i * 4 +2];
    }
      if(grey>rageGrey)
      pixels[i * 4 + 3] = 0;
  }
  ctx2.putImageData(imageData, 0, 0)
}
function download() {
  const mc = document.getElementById('canvasWithoutPng')
  document.getElementById('downloadImg').href = mc.toDataURL("image/png");
  document.getElementById('downloadImg').click()
}